﻿using System;
using System.Collections.Generic;
using caalhp.Core.Contracts;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using EventTypes;
using EventTypes.TestEvents;

namespace ShimmerDriverFaker
{
	/// <summary>
	/// Purpose of ShimmerDriverFaker is to be able to send fake Shimmer
	/// readings to the system.
	/// The console let you choose some predefined events to send
	/// Load real Shimmer unit when available
	/// </summary>
	public class ShimmerDriverFaker : IDeviceDriverCAALHPContract
	{
		public static string Name { get; private set; }
		public static string ShimmerEvent { get; set; }
		private IDeviceDriverHostCAALHPContract _host;
		private int _processId;

		public ShimmerDriverFaker()
		{
			Name = "ShimmerDriverFaker";
			ShimmerEvent = "Unknown";
		}

		public void Notify(KeyValuePair<string, string> notification)
		{
			//throw new System.NotImplementedException();
		}

		public string GetName()
		{
			return Name;
		}

		public bool IsAlive()
		{
			return true;
		}

		public void ShutDown()
		{
			Environment.Exit(0);
		}

		public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
		{
			_host = hostObj;
			_processId = processId;
		}

		public void ReadFakeInput()
		{
			var serial = new Serial();
			var running = true;

			Console.WriteLine();
			Console.WriteLine("You can send fake data from this console");
			Console.WriteLine("All data is of the event type: " + ShimmerEvent);
			Console.WriteLine("List of setting to send with:");
			Console.WriteLine("0: Exit faker");
			Console.WriteLine("1: fall = true");
			Console.WriteLine("2: fall = false");
			Console.WriteLine("3: Real driver");

			while (running)
			{
				var input = Console.ReadLine();

				//Create dummy event
				var fakeShimmerEvent = new DummyFall
				{
					CallerProcessId = 1,
					Condition = 80,
					Data = new List<int>(),
					CallerName = "Shimmer faker"
				};

				//Dertermine what to do with the event
				switch (input)
				{
					case "0":
						Console.WriteLine("Shutting down");
						running = false;
						break;

					case "1":
						Console.WriteLine("Send a 'fall'");
						fakeShimmerEvent.Condition = 90;
						SendEvent(fakeShimmerEvent);
						break;

					case "2":
						Console.WriteLine("Send a 'did not fall'");
						fakeShimmerEvent.Condition = 0;
						SendEvent(fakeShimmerEvent);
						break;

					case "3":
						Console.WriteLine("Starting a real connection to a Shimmer unit");
						Connect(serial);
						break;
				}
			}
		}

		private void SendEvent(BaseEvent eventToSend)
		{
			_host.Host.ReportEvent(EventHelper
				.CreateEvent(SerializationType.Json, eventToSend, "EventTypes"));
		}

		void Connect(Serial connection)
		{
			Console.WriteLine("Type COM port:");
			Console.Write("COM:");
			var COMport = "COM" + Console.ReadLine();
			Console.WriteLine("Trying to connect to " + COMport);

			if (connection.OpenPort(COMport))
			{
				Console.WriteLine("\n--------------------------------------");
				Console.WriteLine("Connected to " + COMport);
				Console.WriteLine("--------------------------------------\n");
				Console.WriteLine("Type 'q' and hit Enter to exit the program");
				connection.dataRecieved += OnDataReceived;
			}
			else
				Console.WriteLine("Unable to connect to specified COM port\n");
		}

		void OnDataReceived(object sender, DataEventArgs e)
		{
			byte[] data = e.Data;

			if (data[0] == 'F')
			{
				Console.WriteLine("ALERT: A fall has occured");
				var shimmerEvent = new Shimmer
				{
					CallerProcessId = _processId,
					Data = new List<int>(),
					Condition = 100,
					CallerName = "Shimmer unit"
				};
				SendEvent(shimmerEvent);
			}
		}
	}
}
